package fr.ghocleme.model;

import fr.ghocleme.exception.CarRegistrationException;

import java.util.ArrayList;
import java.util.List;

public class CarManager {

    private final List<Car> cars;

    public CarManager() {
        this.cars = new ArrayList<>();
    }

    public void addCar(Car car) {

        if(this.hasCar(car))
            throw new CarRegistrationException("Car already registered.");

        this.cars.add(car);
    }

    public void removeCar(Car car) {

        if(!this.hasCar(car))
            throw new CarRegistrationException("Car not registered.");

        this.cars.remove(car);
    }

    public boolean hasCar(Car car) {
        return this.cars.contains(car);
    }

    public int countCars() {
        return this.cars.size();
    }

    public List<Car> getCars() {
        return this.cars;
    }
}
