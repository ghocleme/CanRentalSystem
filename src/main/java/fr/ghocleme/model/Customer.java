package fr.ghocleme.model;

import java.util.Objects;

public class Customer {

    private final String firstname, lastname;
    private final String phone;

    public Customer(String firstname, String lastname, String phone) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return firstname.equals(customer.firstname) && lastname.equals(customer.lastname) && phone.equals(customer.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstname, lastname, phone);
    }
}
