package fr.ghocleme.model;

import java.util.Objects;

public class Car {

    private final String brand;
    private final String registration;

    public Car(String brand, String registration) {
        this.brand = brand;
        this.registration = registration;
    }

    public String getBrand() {
        return this.brand;
    }

    public String getRegistration() {
        return this.registration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return brand.equals(car.brand) && registration.equals(car.registration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, registration);
    }
}
