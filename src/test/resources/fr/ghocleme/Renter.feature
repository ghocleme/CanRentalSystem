Feature: Manage the stock of cars

  As a renter
  I want to be able to manage my stocks of cars
  So I can rent them to customers

  Scenario: Add a new car
    Given there are 0 cars in the list
    When I add a new car
    Then I should see the car in my car list
    And the number of cars should be increased by 1

  Scenario: Register a car twice
    Given there are 3 cars in the list
    When I try to add a car that already exists
    Then the car should not be added

  Scenario: Remove an existing car
    Given there are 2 cars in the list
    When I try to remove a car from the list
    Then the car should be removed
    And the number of cars decreased by 1

  Scenario: Remove a non-registered car
    Given a car not in the list
    And there are 3 cars in the list
    When I try to remove this car from the list
    Then no car should be removed

  # @Manual
  # Scenario: Show all cars
    # Given there are 3 cars in the list
    # When I want to see all the cars of the list
    # Then I should see 3 cars