package fr.ghocleme;

import fr.ghocleme.exception.CarRentalException;
import fr.ghocleme.model.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class RentalStepsDefinition {

    private CarRentalSystem system;

    private Customer customer;
    private Car car, anotherCar;
    private boolean rent;

    //  Background

    @Given("the car rental system is running")
    public void theCarRentalSystemIsRunning() {
        CarManager manager = new CarManager();
        this.system = new CarRentalSystem(manager);
    }

    @And("there are available cars in the stock")
    public void thereAreAvailableCarsInTheStock() {

        Car c1 = new Car("Renault", "AB-123-CD");
        Car c2 = new Car("Peugeot", "AB-456-CD");
        Car c3 = new Car("Ferrari", "AB-789-CD");

        CarManager manager = this.system.getCarManager();
        manager.addCar(c1);
        manager.addCar(c2);
        manager.addCar(c3);

        this.car = c3;
    }

    @And("a customer")
    public void aCustomer() {
        this.customer = new Customer("John", "Smith", "0603030303");
    }

    // Scenario : Rent a car.

    @When("The customer select a car to rent")
    public void theCustomerSelectACarToRent() {
        this.system.rentCar(this.customer, this.car, 3);
    }

    @Then("the car should not be available anymore")
    public void theCarShouldNotBeAvailableAnymore() {
        Assert.assertFalse(this.system.isCarAvailable(this.car));
    }

    @And("the rental should be added to the rentals list")
    public void theRentalShouldBeAddedToTheRentalsList() {

        Assert.assertTrue(this.system.hasRental(this.customer));

        Rental rental = this.system.getRental(this.customer);

        Assert.assertEquals(rental.getCar(), this.car);
        Assert.assertEquals(rental.getCustomer(), this.customer);
    }

    // Scenario : Return a car

    @Given("the customer has a rental for {int} days")
    public void theCustomerHasARental(int arg0) {
        this.system.rentCar(this.customer, this.car, arg0);
    }

    @When("The customer returns the car")
    public void theCustomerReturnsTheCar() {
        this.system.returnCar(this.customer);
    }

    @Then("the car should now be available")
    public void theCarShouldNowBeAvailable() {
        Assert.assertTrue(this.system.isCarAvailable(this.car));
    }

    @And("the rental should be removed from the rentals list")
    public void theRentalShouldBeRemovedFromTheRentalsList() {
        Assert.assertFalse(this.system.hasRental(this.customer));
    }

    // Scenarios : Extend a rental

    @When("he wants to extend it by {int} days")
    public void heWantsToExtendItByDays(int arg0) {
        try {
            this.system.prolongRental(this.customer, arg0);
        } catch (CarRentalException ignored) {}
    }

    @Then("the rental should be extended to {int} days")
    public void theRentalShouldBeExtendedToDays(int arg0) {
        Rental rental = this.system.getRental(this.customer);
        Assert.assertEquals(rental.getDays(), arg0);
    }

    @Then("the rental should not be extended")
    public void theRentalShouldNotBeExtended() {
        Rental rental = this.system.getRental(this.customer);
        Assert.assertTrue(rental.getDays() <= 10);
    }

    // Scenario : Rent two cars

    @When("The customer select another car to rent")
    public void theCustomerSelectAnotherCarToRent() {
        this.anotherCar = this.system.getCarManager().getCars().get(2);

        try {
            this.system.rentCar(this.customer, this.anotherCar, 2);
            this.rent = true;
        } catch (CarRentalException ignored) { this.rent = false; }
    }

    @Then("then rental should not be allowed")
    public void thenRentalShouldNotBeAllowed() {
        Assert.assertFalse(this.rent);
    }
}
