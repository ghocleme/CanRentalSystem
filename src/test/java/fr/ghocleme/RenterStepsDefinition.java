package fr.ghocleme;

import fr.ghocleme.exception.CarRegistrationException;
import fr.ghocleme.model.Car;
import fr.ghocleme.model.CarManager;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class RenterStepsDefinition {

    private CarManager manager;
    private int countCars;
    private Car car;

    @Given("there are {int} cars in the list")
    public void thereAreCarInTheList(int arg0) {

        this.manager = new CarManager();

        for(int i = 0; i < arg0; i++) {
            Car car = new Car("Peugeot", "AB-" + 100*i + "-CD");
            this.manager.addCar(car);
        }

        this.countCars = this.manager.countCars();
    }

    // Scenario : Add a new car

    @When("I add a new car")
    public void iAddANewCar() {
        this.car = new Car("Peugeot", "AB-123-CD");
        this.manager.addCar(this.car);
    }

    @Then("I should see the car in my car list")
    public void iShouldSeeTheCarInMyCarList() {
        Assert.assertTrue(this.manager.hasCar(this.car));
    }

    @And("the number of cars should be increased by {int}")
    public void theNumberOfCarsShouldBeIncreasedBy(int arg0) {
        Assert.assertEquals(this.countCars + arg0, this.manager.countCars());
    }

    // Scenario : Register a car twice

    @When("I try to add a car that already exists")
    public void iTryToAddACarThatAlreadyExists() {
        Car car = this.manager.getCars().get(0);
        try { this.manager.addCar(car);
        } catch (CarRegistrationException ignored) {}
    }

    @Then("the car should not be added")
    public void theCarShouldNotBeAdded() {
        Assert.assertEquals(this.countCars, this.manager.countCars());
    }

    // Scenario : Remove an existing car

    @When("I try to remove a car from the list")
    public void iTryToRemoveACarFromTheList() {
        this.car = this.manager.getCars().get(0);
        this.manager.removeCar(this.car);
    }

    @Then("the car should be removed")
    public void theCarShouldBeRemoved() {
        Assert.assertFalse(this.manager.hasCar(this.car));
    }

    @And("the number of cars decreased by {int}")
    public void theNumberOfCarsDecreasedBy(int arg0) {
        Assert.assertEquals(this.countCars - arg0, this.manager.countCars());
    }

    // Scenario : Remove a non-registered car

    @Given("a car not in the list")
    public void aCarNotInTheList() {
        this.car = new Car("Aston Martin", "AB-007-CD");
    }

    @When("I try to remove this car from the list")
    public void iTryToRemoveThisCarFromTheList() {
        try { this.manager.removeCar(this.car);
        } catch (CarRegistrationException ignored) {}
    }

    @Then("no car should be removed")
    public void noCarShouldBeRemoved() {
        Assert.assertEquals(this.countCars, this.manager.countCars());
    }
}
